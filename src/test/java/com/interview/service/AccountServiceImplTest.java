package com.interview.service;

import com.interview.api.exception.InvalidAmountException;
import com.interview.dao.AccountDao;
import com.interview.entity.Account;
import com.interview.entity.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AccountServiceImplTest {

    private final AccountDao accountDao = mock(AccountDao.class);

    @InjectMocks
    private final AccountService accountService = new AccountServiceImpl(accountDao);
    private Account firstAccount;
    private Account secondAccount;

    @Before
    public void setup() {
        firstAccount = mockAccount("1", 200);
        secondAccount = mockAccount("2", 200);
        when(accountDao.findAccount("1")).thenReturn(Optional.of(firstAccount));
        when(accountDao.findAccount("2")).thenReturn(Optional.of(secondAccount));
    }

    @Test
    public void transfer() {
        final ArgumentCaptor<Transaction> withdrawTransaction = ArgumentCaptor.forClass(Transaction.class);
        final ArgumentCaptor<Transaction> depositTransaction = ArgumentCaptor.forClass(Transaction.class);
        accountService.transfer(20, firstAccount, secondAccount);
        verify(accountDao, times(1)).transfer(withdrawTransaction.capture(), depositTransaction.capture());
        assertEquals("1", withdrawTransaction.getValue().getAccount().getId());
        assertEquals("2", depositTransaction.getValue().getAccount().getId());
        assertEquals(depositTransaction.getValue().getAmount(), depositTransaction.getValue().getAmount());
    }

    @Test
    public void transferInvalidCase() {
        when(firstAccount.getBalance()).thenReturn(10);
        try {
            accountService.transfer(20, firstAccount, secondAccount);
        } catch (final Exception exception) {
            assertTrue(exception instanceof InvalidAmountException);
        } finally {
            verifyNoMoreInteractions(accountDao);
        }
    }

    @Test
    public void findAccount() {
        final Optional<Account> maybeAccount = accountService.findAccount("1");
        assertTrue(maybeAccount.isPresent());
        final Account account = maybeAccount.get();
        assertEquals("Balance must be equal to original balance", 200, (int) account.getBalance());
        assertFalse("Invalid id should return empty optional", accountService.findAccount("blah").isPresent());
    }

    @Test
    public void getAllAccounts() {
        when(accountDao.getAllAccounts()).thenReturn(Arrays.asList(firstAccount, secondAccount));
        assertEquals("There must be two accounts", 2, accountService.getAllAccounts().size());
    }

    private Account mockAccount(final String id, final int balance) {
        final Account account = mock(Account.class);
        when(account.getId()).thenReturn(id);
        when(account.getBalance()).thenReturn(balance);
        return account;
    }
}
