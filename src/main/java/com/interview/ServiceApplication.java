package com.interview;

import com.interview.api.AccountResource;
import com.interview.dao.AccountDao;
import com.interview.dao.AccountDaoImpl;
import com.interview.dao.AccountResultMapper;
import com.interview.dao.TransactionResultMapper;
import com.interview.service.AccountService;
import com.interview.service.AccountServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

/**
 * http://www.dropwizard.io/1.0.6/docs/manual/core.html#application
 */
public class ServiceApplication extends Application<ServiceConfiguration> {
    public static void main(final String[] args) throws Exception {
        new ServiceApplication().run(args);
    }

    @Override
    public void initialize(final Bootstrap<ServiceConfiguration> bootstrap) {
        super.initialize(bootstrap);
        bootstrap.addBundle(new FlywayBundle<ServiceConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(final ServiceConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }

            @Override
            public FlywayFactory getFlywayFactory(final ServiceConfiguration configuration) {
                return configuration.getFlywayFactory();
            }
        });
    }

    @Override
    public void run(final ServiceConfiguration config, final Environment env) throws Exception {

        // cleans database
        config.getFlywayFactory().build(config.getDataSourceFactory().build(env.metrics(), "db")).clean();

        // re-run flyway migrations
        config.getFlywayFactory().build(config.getDataSourceFactory().build(env.metrics(), "db")).migrate();


        // Datasource factory, jdbi connections
        final DBIFactory factory = new DBIFactory();
        final DBI dbi = factory.build(env, config.getDataSourceFactory(), "postgresql");

        // Register Result Set Mapper for POJO Account with UUID id.
        final AccountResultMapper accountResultMapper = new AccountResultMapper();
        dbi.registerMapper(accountResultMapper);
        dbi.registerMapper(new TransactionResultMapper());


        final AccountDao accountDao = new AccountDaoImpl(dbi);
        final AccountService accountService = new AccountServiceImpl(accountDao);
        final AccountResource accountResource = new AccountResource(accountService);

        //register your API resource here
        env.jersey().register(accountResource);
    }
}
