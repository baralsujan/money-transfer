package com.interview.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Stores information about account. Account has a balance which can increase or decrease
 * depending on deposits or withdraws to and from account. It gives a snapshot of current
 * balance.
 *
 * @see Transaction
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Account {

    private String id;
    private Integer balance;

    public Account(final String id, final Integer balance) {
        this.id = id;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(final Integer balance) {
        this.balance = balance;
    }
}
