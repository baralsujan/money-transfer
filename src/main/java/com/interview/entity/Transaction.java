package com.interview.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.DateTime;

/**
 * Stores information about incoming and outgoing amounts to and from accounts.
 * Whenever the balance of an account changes, there will be a corresponding
 * transaction which resulted in the balane change.
 *
 * @see Account
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Transaction {
    private Long id;
    private Account account;
    private Integer amount;
    private Type type;
    private Status status;
    private DateTime created;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(final Account account) {
        this.account = account;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(final Integer amount) {
        this.amount = amount;
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(final DateTime created) {
        this.created = created;
    }

    /**
     * Indicates the type of transaction that can happen to the account balance
     */
    public enum Type {
        /**
         * Indicates that as a result of this type of transaction, the resulting balance in account
         * is guaranteed to be more that the balance before the transaction.
         */
        CREDIT,

        /**
         * Indicates that as a result of this type of transaction, the resulting balance in account
         * is guaranteed to be less that the balance before the transaction.
         */
        DEBIT
    }

    /**
     * Stores different status a transaction can be in
     */
    public enum Status {
        /**
         * Indicates that the transaction has been started but no further actions have been done
         * A transaction with this status doesn't change the balance of an account.
         */
        PENDING,

        /**
         * Indicates that the transaction has been completed. A transaction in this status indicates
         * that the associated account will have changes to the balance.
         */
        COMPLETE,

        /**
         * Indicates that the transaction falied. A transaction in this status indicates that there has
         * been no change in the balances of any account involved.
         */
        FAILED
    }
}
