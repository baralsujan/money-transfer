package com.interview.dao;

import com.interview.entity.Account;
import com.interview.entity.Transaction;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.Update;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * http://www.dropwizard.io/1.0.6/docs/manual/jdbi.html
 * http://jdbi.org/dbi_handle_and_statement/
 */
public class AccountDaoImpl implements AccountDao {

    private final DBI dbi;

    public AccountDaoImpl(final DBI dbi) {
        this.dbi = dbi;
    }

    @Override
    public List<Account> getAllAccounts() {
        try (final Handle h = dbi.open()) {
            return h.createQuery("SELECT * FROM accounts;")
                    .mapTo(Account.class)
                    .list();
        }
    }

    @Override
    public Optional<Account> findAccount(final String id) {
        try (final Handle h = dbi.open()) {
            return Optional.ofNullable(h.createQuery("SELECT * FROM accounts " +
                    " WHERE id=:id;")
                    .bind("id", id)
                    .mapTo(Account.class)
                    .first());
        }
    }

    @Override
    public Transaction createTransaction(final Transaction transaction) {
        return createTransaction(transaction, dbi.open());
    }

    private Transaction createTransaction(final Transaction transaction, final Handle handle) {
        final boolean isInTransaction = handle.isInTransaction();
        if (!isInTransaction) {
            handle.begin();
        }
        switch (transaction.getType()) {
            case DEBIT:
                withdrawBalance(transaction.getAccount(), transaction.getAmount(), handle);
                break;
            case CREDIT:
                addBalance(transaction.getAccount(), transaction.getAmount(), handle);
                break;
            default:
                throw new IllegalStateException("Unknown transaction type.");
        }
        final Map map = handle.createStatement("INSERT INTO transactions(account_id, amount, transaction_type, status, created) "
                + "VALUES (:accountId, :amount, :transactionType, :status, now())")
                .bind("accountId", transaction.getAccount().getId())
                .bind("amount", transaction.getAmount())
                .bind("transactionType", transaction.getType().name())
                .bind("status", Transaction.Status.COMPLETE.name())
                .executeAndReturnGeneratedKeys()
                .first();
        if (!isInTransaction) {
            handle.commit();
        }
        transaction.setId(Long.valueOf(map.get("id").toString()));
        return transaction;
    }

    @Override
    public void transfer(final Transaction debit, final Transaction credit) {
        final Handle handle = dbi.open();
        handle.begin();
        createTransaction(debit, handle);
        createTransaction(credit, handle);
        handle.commit();
        handle.close();
    }

    @Override
    public List<Transaction> getTransactions(final Account account) {
        try (final Handle h = dbi.open()) {
            return h.createQuery("SELECT * FROM transactions " +
                    "JOIN accounts ON accounts.id = transactions.account_id " +
                    "WHERE account_id = :accountId")
                    .bind("accountId", account.getId())
                    .mapTo(Transaction.class)
                    .list();
        }
    }

    private void withdrawBalance(final Account account, final Integer amount, final Handle handle) {
        final Update query = handle.createStatement("UPDATE accounts SET balance = (balance - " +
                ":amount) WHERE id = :id AND balance >= :amount")
                .bind("amount", amount)
                .bind("id", account.getId());
        final int rows;
        synchronized (this) {
            rows = query.execute();
        }
        if (rows != 1) {
            handle.close();
            throw new IllegalStateException("Not enough balance");
        }
    }

    private void addBalance(final Account account, final Integer amount, final Handle handle) {
        handle.createStatement("UPDATE accounts SET balance = (balance + " +
                ":amount) WHERE id = :id")
                .bind("amount", amount)
                .bind("id", account.getId())
                .execute();
    }
}
