package com.interview.dao;

import com.interview.entity.Account;
import com.interview.entity.Transaction;

import java.util.List;
import java.util.Optional;

public interface AccountDao {
    List<Account> getAllAccounts();

    Optional<Account> findAccount(String id);

    Transaction createTransaction(Transaction transaction);

    void transfer(Transaction debit, Transaction credit);

    List<Transaction> getTransactions(Account account);
}
