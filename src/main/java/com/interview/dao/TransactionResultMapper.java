package com.interview.dao;

import com.interview.entity.Account;
import com.interview.entity.Transaction;
import org.joda.time.DateTime;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionResultMapper implements ResultSetMapper<Transaction> {

    @Override
    public Transaction map(final int index, final ResultSet r, final StatementContext ctx) throws SQLException {
        final Transaction transaction = new Transaction();
        transaction.setId(r.getLong("id"));
        transaction.setType(Transaction.Type.valueOf(r.getString("transaction_type").trim()));
        transaction.setStatus(Transaction.Status.valueOf(r.getString("status").trim()));
        transaction.setAmount(r.getInt("amount"));
        transaction.setCreated(new DateTime(r.getDate("created")));
        transaction.setAccount(new Account(r.getString("id"), null));
        return transaction;
    }
}
