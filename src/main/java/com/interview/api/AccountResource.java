package com.interview.api;

import com.interview.api.exception.AccountNotFoundException;
import com.interview.api.exception.InvalidAmountException;
import com.interview.entity.Account;
import com.interview.entity.Transaction;
import com.interview.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * http://www.dropwizard.io/1.0.6/docs/manual/core.html#resources
 */
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountService accountService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

    public AccountResource(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("ping")
    public String ping() {
        return "Pong";
    }

    @GET
    @Path("/accounts")
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @GET
    @Path("/accounts/{accountId}")
    public Account getAccount(@PathParam("accountId") final String accountId) throws AccountNotFoundException {
        return accountService.findAccount(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    /*
     * Ideally this method would be void but have returned the current state of accounts to quickly verify the results of transfer
     */
    @POST
    @Path("/accounts/transfer")
    public List<Account> transfer(final TransferRequest transferRequest) throws AccountNotFoundException, InvalidAmountException {
        LOGGER.info("Received: Transfer request from {} to {} for amount {}",
                transferRequest.getSource(),
                transferRequest.getDestination(),
                transferRequest.getAmount());
        if (transferRequest.getAmount() <= 0) {
            throw new InvalidAmountException("Amount cannot be negative.");
        }
        final Account source = getAccount(transferRequest.getSource());
        final Account destination = getAccount(transferRequest.getDestination());

        accountService.transfer(transferRequest.getAmount(), source, destination);
        LOGGER.info("Completed: Transfer request from {} to {} for amount {}",
                transferRequest.getSource(),
                transferRequest.getDestination(),
                transferRequest.getAmount());
        return getAllAccounts();
    }

    @GET
    @Path("/accounts/{accountId}/transactions")
    public List<Transaction> getTransactions(@PathParam("accountId") final String accountId) throws AccountNotFoundException {
        return accountService.getTransactions(getAccount(accountId));
    }

}
