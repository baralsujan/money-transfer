package com.interview.api.exception;

import javax.ws.rs.WebApplicationException;

/**
 * Indicates that there has been an error due to failure to locate an account
 */
public class AccountNotFoundException extends WebApplicationException {

    public AccountNotFoundException(final String accountId) {
        super(String.format("Could not find account with id %s", accountId));
    }
}
