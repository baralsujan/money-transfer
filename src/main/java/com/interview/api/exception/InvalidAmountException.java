package com.interview.api.exception;

import javax.ws.rs.WebApplicationException;

/**
 * Indicates there has been an error due to the amount being invalid. Cases of invalid amounts could be
 * negative amounts or too high amount.
 */
public class InvalidAmountException extends WebApplicationException {

    public InvalidAmountException(String message) {
        super(message);
    }
}
