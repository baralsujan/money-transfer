package com.interview.service;

import com.interview.api.exception.InvalidAmountException;
import com.interview.dao.AccountDao;
import com.interview.entity.Account;
import com.interview.entity.Transaction;
import org.joda.time.Instant;

import java.util.List;
import java.util.Optional;

public class AccountServiceImpl implements AccountService {

    private final AccountDao accountDao;

    public AccountServiceImpl(final AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void transfer(final Integer amount, final Account source, final Account destination) {
        if (amount > source.getBalance()) {
            throw new InvalidAmountException("There is not enough balance to transfer");
        }
        accountDao.transfer(createTransaction(amount, source, Transaction.Type.DEBIT),
                createTransaction(amount, destination, Transaction.Type.CREDIT));
    }

    @Override
    public Optional<Account> findAccount(final String accountId) {
        return accountDao.findAccount(accountId);
    }

    @Override
    public List<Transaction> getTransactions(final Account account) {
        return accountDao.getTransactions(account);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountDao.getAllAccounts();
    }

    private Transaction createTransaction(final Integer amount, final Account account, final Transaction.Type type) {
        final Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(amount);
        transaction.setCreated(Instant.now().toDateTime());
        transaction.setType(type);
        return transaction;
    }
}
