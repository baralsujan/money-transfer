package com.interview.service;

import com.interview.api.exception.InvalidAmountException;
import com.interview.entity.Account;
import com.interview.entity.Transaction;

import java.util.List;
import java.util.Optional;

/**
 * Provides methods to interact with account.
 */
public interface AccountService {

    /**
     * <p>Withdraws money from {@code source} account and deposits to {@code destination} account. The transfer
     * happens in two steps
     * <ul>
     *     <li>Withdraws {@code amount} from {@code source} account</li>
     *     <li>Deposits {@code amount} to {@code destination} account</li>
     * </ul>
     * <p>The transaction can't go through in either of cases </p>
     * <ul>
     *     <li>The {@code source} account doesn't have enough amount</li>
     *     <li>Fails to create {@link Transaction} for any of withdraw or deposit event</li>
     * </ul>
     * </p>
     * @param amount which will be transferred
     * @param source account from which amount will be transferred
     * @param destination to which money will be transferred
     * @throws InvalidAmountException if {@code source} account balance is less than {@code amount}
     */
    void transfer(Integer amount, Account source, Account destination) throws InvalidAmountException;

    /**
     * Searches for account for given {@code accountId} and returns it.
     * @param accountId for which account will be looked for
     * @return {@code Optional} of account if found, empty {@code Optional} otherwise
     */
    Optional<Account> findAccount(String accountId);

    /**
     * Returns all the transactions for given account.
     * @param account for which transactions will be returned
     * @return a list of transactions when found, empty list otherwise
     */
    List<Transaction> getTransactions(Account account);

    /**
     * Returns all accounts in the system
     * @return list of accounts when found, empty list otherwise
     */
    List<Account> getAllAccounts();
}
