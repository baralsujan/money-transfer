CREATE TABLE IF NOT EXISTS accounts
(
  id        TEXT UNIQUE NOT NULL,
  balance   int NOT NULL DEFAULT 0,
  CONSTRAINT pk_accounts_id PRIMARY KEY (id)
);


INSERT INTO accounts
(id, balance)
VALUES
('1', 100),
('2', 120),
('3', 700),
('4', 1000);


CREATE TABLE IF NOT EXISTS transactions
(
  id                SERIAL,
  account_id        TEXT NOT NULL,
  amount            int NOT NULL DEFAULT 0,
  transaction_type  char(50) NOT NULL,
  status            char(50) NOT NULL,
  created           TIMESTAMP,
  CONSTRAINT pk_id PRIMARY KEY (id)
);